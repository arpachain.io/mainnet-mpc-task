from Compiler.types import *
from Compiler.library import *

# print functions
def printMat(original_matrix):    
    if type(original_matrix) != sfixMatrix:
        print_ln("%s is not a matrix",type(original_matrix))
    else:
        print_ln("Printing matrix...")
        original_row = len(original_matrix)
        original_column = len(original_matrix[0])
        @for_range(original_row)
        def loop_body(i):
            @for_range(original_column)
            def loop_body(j):
                if type(original_matrix[i][j]) == type(sfix(0)):
                    print_str("%s,\t", original_matrix[i][j].reveal())
                else:
                    print_str("%s,\t", original_matrix[i][j])
            print_str("\n")

def printArray(original_array):
    if type(original_array) not in {sfixArray, Array}:
        print_ln("%s is not a array",type(original_array))
    else:
        print_ln("Printing array...")
        original_length = len(original_array)
        @for_range(original_length)
        def loop_body(i):
            print_str("%s,\t",original_array[i].reveal())
        print_str("\n")

