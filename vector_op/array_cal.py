from Compiler.types import *
from Compiler.library import *

# array functions
def sfixArrayMult(left_array, right_array):
    left_array_length = len(left_array)
    right_array_length = len(right_array)
    if left_array_length != right_array_length:
        print_ln("Array multiplication: dimension not matched")
        res = regint(0)
    else:
        print_ln("Array multiplication...")
        res_array = sfix.Array(1)
        res_array[0] = sfix(0)
        @for_range(left_array_length)
        def loop_body(i):
            res_array[0] = res_array[0] + left_array[i] * right_array[i]
        res = res_array[0]
    return res

def sfixArrayDotOp(left_array, right_array, op_code):
    if op_code in ["add", "sub", "mult", "div"]:
        left_array_length = len(left_array)
        right_array_length = len(right_array)
        if left_array_length != right_array_length:
            print_ln("Array dot operation: dimension not matched")
            res_array = regint(0)
        else:
            print_ln("Array dot operation " + op_code + "...")
            res_array = sfix.Array(left_array_length)
            @for_range(left_array_length)
            def loop_body(i):
                if op_code == 'add':
                    res_array[i] = left_array[i] + right_array[i]
                elif op_code == 'sub':
                    res_array[i] = left_array[i] - right_array[i]
                elif op_code == 'mult':
                    res_array[i] = left_array[i] * right_array[i]
                elif op_code == 'div':
                    res_array[i] = left_array[i] / right_array[i]
    else:
        res_array = regint(0)
        print_ln("Operation not supported")
    return res_array

def sfixArrayMatMult(left_array, right_matrix):
    left_array_length = len(left_array)
    right_matrix_row = len(right_matrix)
    right_matrix_column = len(right_matrix[0])
    if right_matrix_row != left_array_length:
        print_ln("Array mat multiplication: dimension not matched")
        res_array = regint(0)
    else:
        print_ln("Array mat multiplication...")
        res_array = sfix.Array(right_matrix_column)
        @for_range(right_matrix_column)
        def loop_body(i):
            res_array[i] = sfix(0)
            @for_range(left_array_length)
            def loop_body(j):
                res_array[i] = res_array[i] + left_array[j] * right_matrix[j][i]
    return res_array

def sfixArrayRepmat(original_array, column):
    original_array_length = len(original_array)
    print_ln("Array repeats to build matrix...")
    res_matrix = sfix.Matrix(original_array_length, column)
    @for_range(original_array_length)
    def loop_body(i):
        @for_range(column)
        def loop_body(j):
            res_matrix[i][j] = original_array[i]
    return res_matrix

def sfixArraySum(original_array):
    original_array_length = len(original_array)
    print_ln("Array Summation")
    res_array = sfix.Array(1)
    res_array[0] = sfix(0)
    @for_range(original_array_length)
    def loop_body(i):
        res_array[0] = res_array[0] + original_array[i]
    return res_array[0]

def sintArrayEqual(left_array, right_array):
    left_array_length = len(left_array)
    right_array_length = len(right_array)
    flag = cint(0)
    counter = sint.Array(1)

    if left_array_length != right_array_length:
        print_ln("Array equlity operation: dimension not matched")
        flag = cint(0)
    else:
        counter[0] = sint(0)
        @for_range(left_array_length)
        def loop_body(i):
            counter[0] = counter[0] + (left_array[i] == right_array[i])
        if_then((counter[0] == sint(left_array_length)).reveal())
        flag = cint(1)
        else_then()
        flag = cint(0)
        end_if()
 
    return flag


# def sintArrayEqual(left_array, right_array):
#     left_array_length = len(left_array)
#     right_array_length = len(right_array)
#     flag = cint.Array(1)
#
#     if left_array_length != right_array_length:
#         print_ln("Array equlity operation: dimension not matched")
#         flag[0] = cint(0)
#     else:
#         flag[0] = cint(1)
#         @for_range(left_array_length)
#         def loop_body(i):
#             if_then((left_array[i] != right_array[i]).reveal() + (flag[0] == cint(1)) == cint(2))
#             flag[0] = cint(0)
#             end_if()
#     return flag[0]

