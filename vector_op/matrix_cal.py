from Compiler.types import *
from Compiler.library import *


# matrix functions
def sfixMatMult(left_matrix, right_matrix):
    left_matrix_row = len(left_matrix)
    left_matrix_column = len(left_matrix[0])
    right_matrix_row = len(right_matrix)
    right_matrix_column = len(right_matrix[0])
    if left_matrix_column != right_matrix_row:
        print_ln("Matrix multiplication: dimension not matched")
        res_matrix = regint(0)
    else:
        print_ln("Matrix multiplication...")
        res_matrix = sfix.Matrix(left_matrix_row, right_matrix_column)
        @for_range(left_matrix_row)
        def loop_body(i):
            @for_range(right_matrix_column)
            def loop_body(j):
                res_matrix[i][j] = sfix(0)
                @for_range(left_matrix_column)
                def loop_body(k):
                    res_matrix[i][j] = res_matrix[i][j] + left_matrix[i][k] * right_matrix[k][j]
    return res_matrix

def sfixMatDotOp(left_matrix, right_matrix, op_code):
    if op_code in ['add','sub','mult','div']:
        left_matrix_row = len(left_matrix)
        left_matrix_column = len(left_matrix[0])
        right_matrix_row = len(right_matrix)
        right_matrix_column = len(right_matrix[0])
        if left_matrix_row != right_matrix_row or left_matrix_column != right_matrix_column:
            print_ln("Matrix dot operations: dimension not matched")
            res_matrix = regint(0)
        else:
            print_ln('Matrix dot operations ' + op_code + '...')
            res_matrix = sfix.Matrix(left_matrix_row, right_matrix_column)
            @for_range(left_matrix_row)
            def loop_body(i):
                @for_range(left_matrix_column)
                def loop_body(j):
                    if op_code == 'add':
                        res_matrix[i][j] = left_matrix[i][j] + right_matrix[i][j]
                    elif op_code == 'sub':
                        res_matrix[i][j] = left_matrix[i][j] - right_matrix[i][j]
                    elif op_code == 'mult':
                        res_matrix[i][j] = left_matrix[i][j] * right_matrix[i][j]
                    elif op_code == 'div':
                        res_matrix[i][j] = left_matrix[i][j] / right_matrix[i][j]
    else:
        res_matrix = regint(0)
        print_ln("Operation not supported")
    return res_matrix

def sfixMatTrans(original_matrix):
    original_row = len(original_matrix)
    original_column = len(original_matrix[0])
    res_matrix = sfix.Matrix(original_column, original_row)
    @for_range(original_column)
    def loop_body(i):
        @for_range(original_row)
        def loop_body(j):
           res_matrix[i][j] = original_matrix[j][i]
    return res_matrix

