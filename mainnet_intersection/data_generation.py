import numpy
import random

mat_a_row = 100
column = 11
mat_b_row = 200

intersection_row = 23 

mat_a = numpy.random.randint(0,10,(mat_a_row, column))
mat_b = numpy.random.randint(0,10,(mat_b_row, column))
mat_inter = numpy.zeros((intersection_row, column))
a_index = random.sample(range(0,mat_a_row),intersection_row)
b_index = random.sample(range(0,mat_b_row),intersection_row)
for i in range(intersection_row):
    mat_b[b_index[i],:] = mat_a[a_index[i],:]
    mat_inter[i,:] = mat_a[a_index[i],:]
# print(mat_a)
# print(mat_b)
# for i in range(mat_a_row):
#     for j in range(mat_b_row):
#         if numpy.array_equal(mat_a[i,:], mat_b[j,:]):
#             print (mat_a[i,:])

with open('data_input.mpc','w+') as f, open('file_head.mpc','r') as f_head, open('file_tail.mpc','r') as f_tail:
    for line in f_head:
        f.write(line)

    f.write('mobile_service_provider = sint.Matrix(%s, %s)\n' % (mat_a_row, column))
    f.write('bank_credit = sint.Matrix(%s, %s)\n' % (mat_b_row, column))

    for i in range(mat_a_row):
        for j in range(column):
            f.write('mobile_service_provider[%d][%d] = sint(%d)\n' % (i, j, mat_a[i,j]))
    
    for i in range(mat_b_row):
        for j in range(column):
            f.write('bank_credit[%d][%d] = sint(%d)\n' % (i, j, mat_b[i,j]))

    f.write('\n\'\'\'\n')
    for i in range(intersection_row):
        for j in range(column):
            f.write('%d\t' % mat_inter[i][j])
        f.write('\n')
    f.write('\n\'\'\'\n')

    for line in f_tail:
        f.write(line)
