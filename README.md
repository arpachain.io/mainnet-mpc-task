# arpa_mpc_lib
MPC librarys and programs bulit by ARPA

Performance Benchmark

### Tested
* mainnet_advanced_arith:  84s
* mainnet_basic_arith:  76s
* mainnet_bit_op:  73s
* mainnet_sorting:  83s
* mainnet_lr_pred:  72s
* mainnet_matrix_mult_sint:  132s
* mainnet_trigonometry:  76s 
* mainnet_branching:  74s
* mainnet_stat:  72s

### Failed
* mainnet_search: compilation failed
* mainnet_matrix_mult_sfix: compilation failed
* mainnet_VaR: compilation failed
* mainnet_intersection: compilation failed
